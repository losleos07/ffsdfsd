%f(x) = {2 , 0<x<2; -2, -2<x<0; T=4
x = [-5:0.01:5];
N=1000;
suma = 5/2;
for n = 1:2:N;
  suma = suma + ((10)/(n*pi))*(sin((n*pi*x)/2));
endfor
plot(x, suma, "linewidth",2)
title('Serie de Fourier')
xlabel('x')
ylabel('f(x)')
grid on;
axis([-5,5,-3,3])
legend('f(x)={0 -2<x<0; 5 0<x<2;T=4')
