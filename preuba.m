x =[-4 -2 -2 0 0 2 2 4 ];
y =[ 5 5 0 0 5 5 0 0];
plot(x,y, "linewidth",2);
axis([-10, 10, -1, 4])
title('Grafica de f(x)')
xlabel('x')
ylabel('f(x)')